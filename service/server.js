const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const http = require('http')
const https = require('https')
const fs = require('fs')

const api = require('./routes/api')

const app = express()
const port = 80
const httpsPort = 443
let httpsCredentials = {}

//set to false if you don't have SSL Certs
const enableHTTPS = true

//===============================
//  HTTPS Setup
//===============================
if (enableHTTPS) {
    const httpsPrivateKey = fs.readFileSync('/usr/src/app/cert/privkey.pem', 'utf8');
    const httpsCertificate = fs.readFileSync('/usr/src/app/cert/cert.pem', 'utf8');
    const httpsCA = fs.readFileSync('/usr/src/app/cert/fullchain.pem', 'utf8');

    httpsCredentials = {
        key: httpsPrivateKey,
        cert: httpsCertificate,
        ca: httpsCA
    }

    // HTTPS redirect
    app.use((req, res, next) => {
        if (req.secure) {
            next();
        } else {
            res.redirect('https://' + req.headers.host + req.url);
        }
    });
}

//PROXY Forwarding Setup for IPs
app.enable('trust proxy');

// parse application/json
app.use(bodyParser.json())
app.use(cookieParser())

app.use(express.static('views'))

app.set('view engine', 'ejs')

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

app.use('/', api.getRouter())

let httpServer = http.createServer(app)
let httpsServer

httpServer.listen(80, () => {
    console.log('Express server listening on port 80')
})

if (enableHTTPS) {
    httpsServer = https.createServer(httpsCredentials, app)

    httpsServer.listen(443, () => {
        console.log('Express server listening on port 443');
    })
} else {
    console.log('Not starting server on 443 because HTTPS is disabled')
}

if(enableHTTPS) {
    api.startWebSocketService(httpsServer)
} else {
    api.startWebSocketService(httpServer)
}