window.onload = () => {
    handleCreateBoardButton()
    handleExitButton()
    handleColumnButtons()
    populateBoards()
}

function handleCreateBoardButton() {
    document.querySelector('#create-board-btn').onclick = () => {
        promptCreateBoard()
    }
}

function promptCreateBoard() {
    overlay.className = ''
}

function handleExitButton() {
    let exitBtn = document.querySelector('.exit-btn')
    exitBtn.onclick = () => {
        clearCard()
    }
}

function handleColumnButtons() {
    let container = document.querySelector('#create-card-column-list')

    let removeColumnBtns = document.querySelectorAll('.column-delete-btn-container > span')
    for(let i = 0; i < removeColumnBtns.length; i++) {
        let node = removeColumnBtns[i]
        node.onclick = () => {
            container.removeChild(node.parentElement.parentElement)
        }
    }

    let addColumnBtn = document.querySelector('#create-card-add-column-btn')
    addColumnBtn.onclick = () => {
        let newColumnEntry = document.createElement('div');
        newColumnEntry.className = 'create-card-column-entry'
        /*
        <input class="create-card-input column-name" value="" required>
        <div class="column-delete-btn-container">
            <span>X</span>
        </div>
        */
        newColumnEntry.innerHTML = '<input class="create-card-input column-name" value="" required><div class="column-delete-btn-container"><span>X</span></div>'
        
        let exitBtn = newColumnEntry.querySelector('.column-delete-btn-container > span')
        exitBtn.onclick = () => {
            container.removeChild(exitBtn.parentElement.parentElement)
        }
        
        container.appendChild(newColumnEntry)
    }
}

async function populateBoards() {
    let boards = await sendRequest('GET', `/boards`)
    boards = JSON.parse(boards.response)
    if(boards == undefined || boards.length === 0) {
        return
    }

    document.querySelector('#board-list-container').innerHTML = ''
    for(let i = 0; i < boards.length; i++) {
        showBoard(boards[i])
    }
}

function showBoard(board) {
    let boardCard = document.createElement('div')
    boardCard.className = 'board-card'
    boardCard.innerHTML = board.title
    boardCard.onclick = () => {
        window.location.pathname = `/boards/${board.id}`
    }

    document.querySelector('#board-list-container').appendChild(boardCard)
}

function clearCard() {
    document.querySelector('#create-card-title-input').value = ''
    document.querySelector('#overlay').className = 'hidden'
}

async function submitCard() {
    let title = document.querySelector('#create-card-title-input').value

    let columns = []
    let columnNodes = document.querySelectorAll('.create-card-input.column-name')
    for(let i = 0; i < columnNodes.length; i++) {
        columns.push(columnNodes[i].value)
    }

    if(!columnsAreUnique(columns)) {
        alert('Each column must have a unique name')
    }

    let requestData = {
        title: title,
        columns: columns
    }
    
    let res = await sendRequest('POST', '/boards', [['Content-Type', 'application/json']], requestData)

    let boardId = JSON.parse(res.response).boardId
    window.location.pathname = `/boards/${boardId}`
}

function columnsAreUnique(columns) {
    let trackingArray = []
    for(let i = 0; i < columns.length; i++) {
        if(trackingArray.includes(columns[i].toLocaleLowerCase())) {
            return false;
        }
        trackingArray.push(columns[i].toLocaleLowerCase())
    }
    return true;
}
// ************************************************
// function: sendRequest
//
// method: HTML Method. Ex. GET, POST, PUT...
// url: URL to send request to
// headers: array of header key-value pairs [[key1, val1], [key2, val2],...]
// data: payload if using methods such as POST 
// 
// return type: Promise
// returns: response for request
// 
// description: function that uses xhr to make requests
// As requests should be asynchronous this method returns
// a promise. Expectation is to call this function with
// 'await' from inside an async function
// ************************************************
var sendRequest = function (method, url, headers, data) {
    // Promise is used for an async call that will
    // be waiting on the node server
    return new Promise(resolve => {

        // request will be an xhr
        var req = new XMLHttpRequest();

        // Add event listener for readystate changing to 'done'
        req.onreadystatechange = function () {

            // 4 means the request is done
            if (this.readyState == 4) {

                // I am simply sending the whole response back to
                // the function that called sendRequest()
                resolve(req);
            }
        };

        // after opening request, set headers. Default to json
        // for content-type if not specified
        req.open(method, url, true);

        if(headers !== undefined) {
            for(let i = 0; i < headers.length; i++) {
                if(headers[i].length !== 2) {
                    error('Header key-val mismatch')
                }
                req.setRequestHeader(headers[i][0], headers[i][1])
            }
        } else {
            req.setRequestHeader("Content-Type", "application/json");
        }

        // As this function is used for HTTP methods with and
        // without payloads, I only send the 'data' attribute
        // if it is defined. Otherwise, send no payload
        if (data != undefined) {
            if(typeof(data) === 'object') {
                data = JSON.stringify(data)
            }
            req.send(data)
        } else {
            req.send()
        }
    });
}
