let columnToAdd = ''
let cardToUpdate = ''
let thumbsUpIds = []
let thumbsUpMap = localStorage.getItem('thumbsUpMap')

window.onload = () => {
    openWebSocket(getBoardId());
    handleAddButtons()
    handleExitButton()
    handleHomeButton()
    populateCards()
    loadThumbsUpMap(getBoardId())
}

function handleAddButtons() {
    let nodes = document.querySelectorAll('.add-button')
    for (let i = 0; i < nodes.length; i++) {
        nodes[i].onclick = () => {
            columnToAdd = nodes[i].dataset.column
            addCardToColumn(document.querySelector(`#${nodes[i].dataset.column}-card-container`))
        }
    }
}

function addCardToColumn(column) {
    //hide edit card
    document.querySelector("#edit-card").classList.add('hidden')
    //unhide overlay
    document.querySelector('#overlay').className = ''
    document.querySelector('#add-card').classList.remove('hidden')
    document.getElementById('edit-card-title-input').focus()
}

function editCard() {
    let editCard = document.querySelector('#edit-card');
    //hide edit card
    document.querySelector("#add-card").classList.add('hidden')
    //unhide overlay
    document.querySelector('#overlay').className = ''
    editCard.classList.remove('hidden')
    document.getElementById('add-card-title-input').focus()

    let cardToEdit = document.querySelector(`#card-${cardToUpdate}`)
    let title = cardToEdit.querySelector('.card-header').innerHTML;
    let details = cardToEdit.querySelector('.card-content').innerHTML;

    editCard.querySelector('#edit-card-title-input').value = title;
    editCard.querySelector('#edit-card-content-input').value = details;
}

function handleExitButton() {
    let exitBtns = document.querySelectorAll('.exit-btn')
    for (let i = 0; i < exitBtns.length; i++) {
        exitBtns[i].onclick = () => {
            clearCard()
        }
    }
}

function loadThumbsUpMap(boardId) {
    if (thumbsUpMap) {
        thumbsUpMap = JSON.parse(thumbsUpMap)
        let board = thumbsUpMap[boardId]
        if (!board) {
            thumbsUpMap[boardId] = []
            localStorage.setItem('thumbsUpMap', JSON.stringify(thumbsUpMap))
        }
    } else {
        thumbsUpMap = '{}'
        loadThumbsUpMap(boardId)
    }
}

function clearCard() {
    document.querySelector('#add-card-title-input').value = ''
    document.querySelector('#add-card-content-input').value = ''
    document.querySelector('#edit-card-title-input').value = ''
    document.querySelector('#edit-card-content-input').value = ''
    document.querySelector('#overlay').className = 'hidden'
}

function submitAddCard() {
    let title = document.querySelector('#add-card-title-input').value
    let details = document.querySelector('#add-card-content-input').value
    sendRequest('POST', '/cards', [['Content-Type', 'application/json']], {
        title: title,
        details: details,
        colId: columnToAdd,
        boardId: getBoardId()
    })
    clearCard()
}

function submitEditCard() {
    let title = document.querySelector('#edit-card-title-input').value
    let details = document.querySelector('#edit-card-content-input').value
    sendRequest('PUT', '/cards', [['Content-Type', 'application/json']], {
        title: title,
        details: details,
        cardId: cardToUpdate,
        boardId: getBoardId()
    })
    clearCard()
}

async function populateCards() {
    let cards = await sendRequest('GET', `/cards?boardId=${getBoardId()}`)
    cards = JSON.parse(cards.response)
    if (cards == undefined) {
        return
    }

    for (let i = 0; i < cards.length; i++) {
        buildCard(cards[i])
    }
}

function buildCard(cardData) {
    let card = document.createElement('div')
    card.className = 'card fade-in-right'
    card.id = 'card-' + cardData._id
    /*
    <div class="card-header">${cardData.title}</div>
    <div class="card-content">${cardData.details}</div>
    <div class="card-footer">
        <div data-id="${cardData._id}" class="footer-btn-container">
            <img src="../res/thumbup.svg" class="thumbsup">
            <div class="thumbsup-count">${cardData.likes}</div>
        </div>
    </div>
    <div data-id="${cardData._id}" class="card-edit-btn">
        <img src="../res/edit.svg" class="edit">
    </div>
    */
    card.innerHTML = `<div class="card-header">${cardData.title}</div><div class="card-content">${cardData.details}</div><div class="card-footer"><div data-id="${cardData._id}" class="footer-btn-container"><img src="../res/thumbup.svg" class="thumbsup"><div class="thumbsup-count">${cardData.likes}</div></div></div><div data-id="${cardData._id}" class="card-edit-btn"><img src="../res/edit.svg" class="edit"></div>`

    let addBtn = card.querySelector('.footer-btn-container')
    addBtn.onclick = () => {
        let id = addBtn.dataset.id
        if (thumbsUpMap[getBoardId()].includes(id)) {
            return
        }

        thumbsUpMap[getBoardId()].push(id)
        localStorage.setItem('thumbsUpMap', JSON.stringify(thumbsUpMap))

        sendRequest('GET', `/thumbsUp/${id}`)
    }

    let editBtn = card.querySelector('.card-edit-btn')
    editBtn.onclick = () => {
        cardToUpdate = cardData._id
        editCard()
    }

    let parent = document.querySelector(`#${cardData.colId}-card-container`)
    if (parent) {
        let addBtn = parent.querySelector('.btn-container')
        parent.insertBefore(card, addBtn)
    }
}

function updateCard(cardData) {
    let card = document.querySelector(`#card-${cardData._id}`)
    if (card) {
        card.querySelector('.card-header').innerHTML = cardData.title
        card.querySelector('.card-content').innerHTML = cardData.details
    }
}

function updateThumbsUp(cardData) {
    let card = document.querySelector(`#card-${cardData._id}`)
    if (card) {
        card.querySelector('.thumbsup-count').innerHTML = cardData.likes
    }
}

function getBoardId() {
    let paths = window.location.pathname.split('/')
    let id = paths[paths.length - 1]

    return (id.indexOf('?') > -1) ? id.substring(0, id.indexOf('?')) : id;
}

function handleHomeButton() {
    document.querySelector('#logo-header').onclick = () => {
        window.location.href = '/'
    }
}

// ************************************************
// function: sendRequest
//
// method: HTML Method. Ex. GET, POST, PUT...
// url: URL to send request to
// headers: array of header key-value pairs [[key1, val1], [key2, val2],...]
// data: payload if using methods such as POST 
// 
// return type: Promise
// returns: response for request
// 
// description: function that uses xhr to make requests
// As requests should be asynchronous this method returns
// a promise. Expectation is to call this function with
// 'await' from inside an async function
// ************************************************
var sendRequest = function (method, url, headers, data) {
    // Promise is used for an async call that will
    // be waiting on the node server
    return new Promise(resolve => {

        // request will be an xhr
        var req = new XMLHttpRequest();

        // Add event listener for readystate changing to 'done'
        req.onreadystatechange = function () {

            // 4 means the request is done
            if (this.readyState == 4) {

                // I am simply sending the whole response back to
                // the function that called sendRequest()
                resolve(req);
            }
        };

        // after opening request, set headers. Default to json
        // for content-type if not specified
        req.open(method, url, true);

        if (headers !== undefined) {
            for (let i = 0; i < headers.length; i++) {
                if (headers[i].length !== 2) {
                    error('Header key-val mismatch')
                }
                req.setRequestHeader(headers[i][0], headers[i][1])
            }
        } else {
            req.setRequestHeader("Content-Type", "application/json");
        }

        // As this function is used for HTTP methods with and
        // without payloads, I only send the 'data' attribute
        // if it is defined. Otherwise, send no payload
        if (data != undefined) {
            if (typeof (data) === 'object') {
                data = JSON.stringify(data)
            }
            req.send(data)
        } else {
            req.send()
        }
    });
}

function openWebSocket(id) {
    let socket = new WebSocket(`wss://${window.location.host}?` + id);

    socket.onopen = function () {
        console.log("Socket connection successful");
    }

    socket.onmessage = function (msg) {
        var request = JSON.parse(msg.data);
        switch (request.type) {
            case 'message':
                alert('message recieved: ' + request.data);
                break;
            case 'addCard':
                buildCard(request.data)
                break
            case 'updateCard':
                updateCard(request.data)
                break
            case 'like':
                updateThumbsUp(request.data)
        }
    };
}