const express = require('express')
const router = express.Router()
const mongoose = require('mongoose');
const WebSocket = require('ws')

const dbUrl = 'mongodb://database/howlr-retro-board';


//===================================
// DB Deetz
//===================================
mongoose.connect(dbUrl);
mongoose.set('useFindAndModify', false);
// create card schema
const cardSchema = new mongoose.Schema({
    title: String,
    details: String,
    likes: Number,
    colId: String,
    boardId: String
});

const Card = mongoose.model('Cards', cardSchema);

// create board schema
const boradSchema = new mongoose.Schema({
    title: String,
    columns: Array,
    id: String,
    owner: String
});

const Board = mongoose.model('Boards', boradSchema);

//===================================
// Web Socket Deetz
//===================================
let clients = {};
let ws;

//===================================
// Endpoints
//===================================
router.get('/', (req, res) => {
    let token = req.cookies.USER_TOKEN
    if (token === undefined) {
        res.cookie('USER_TOKEN', generateToken(64), { expires: new Date(Date.now() + 90000000000) })
    } else {
        //refresh time
        res.cookie('USER_TOKEN', token, { expires: new Date(Date.now() + 90000000000) })
    }
    res.render('splash')
})

router.get('/boards/:id', async (req, res) => {

    Board.findOne({ id: req.params.id }, (err, boards) => {
        if (err) {
            res.status(500).send(error)
            return
        }

        if (boards == null) {
            res.redirect('/')
            return
        }

        addSanitizedColumnNames(boards)

        //res.status(200).json(boards)
        res.render('index', boards)
    });
})

router.get('/boards/:id/columns', async (req, res) => {

    Board.findOne({ id: req.params.id }, (err, boards) => {
        if (err) {
            res.status(500).send(error)
            return
        }

        if (boards == null) {
            res.redirect('/')
            return
        }

        //res.status(200).json(boards)
        res.send(boards.columns)
    });
})

router.get('/boards', (req, res) => {
    Board.find({ 'owner': req.cookies.USER_TOKEN }, (err, boards) => {
        if (err) res.status(500).send(error)

        res.status(200).json(boards);
    });
});

/*
router.get('/boards/:id', (req, res) => {
    Board.findOne({id: req.param.id}, (err, boards) => {
        if (err) res.status(500).send(error)

        res.status(200).json(boards);
    });
});
*/
router.post('/boards', (req, res) => {
    let boardId = generateId();
    console.log("owner", req.cookies.USER_TOKEN)
    let board = new Board({
        title: req.body.title,
        columns: req.body.columns,
        id: boardId,
        owner: req.cookies.USER_TOKEN
    });

    board.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'Board created successfully',
            boardId: boardId
        });
    });
})

router.get('/cards', (req, res) => {
    if (req.query != undefined && req.query.boardId != undefined) {
        Card.find({ boardId: req.query.boardId }, (err, cards) => {
            if (err) res.status(500).send(error)

            res.status(200).json(cards);
        });
    } else {
        Card.find({}, (err, cards) => {
            if (err) res.status(500).send(error)

            res.status(200).json(cards);
        });
    }
});

router.get('/cards/:id', (req, res) => {
    Card.findById(req.params.id, (err, cards) => {
        if (err) res.status(500).send(error)

        res.status(200).json(cards);
    });
});

router.post('/cards', (req, res) => {
    let card = new Card({
        title: req.body.title,
        details: req.body.details,
        likes: 0,
        colId: req.body.colId,
        boardId: req.body.boardId
    });

    card.save(error => {
        if (error) res.status(500).send(error);

        sendCardAddEvent(card)
        res.status(201).json({
            message: 'Card created successfully'
        });
    });
});

router.put('/cards', (req, res) => {
    let cardId = req.body.cardId;
    let updatedCardInfo = {
        title: req.body.title,
        details: req.body.details,
        boardId: req.body.boardId
    }

    Card.findByIdAndUpdate(cardId, updatedCardInfo, (err, card) => {
        if (err) {
            res.send(err)
        } else {
            updatedCardInfo._id = cardId
            sendCardUpdateEvent(updatedCardInfo)
            res.sendStatus(204)
        }
    })
})

router.get('/thumbsUp/:id', async (req, res) => {
    Card.findById(req.params.id, async (err, cards) => {
        if (err) res.status(500).send(error)

        cards.likes = Number(cards.likes) + 1
        await cards.save()

        sendThumbsUpEvent(cards)
        res.sendStatus(200);
    });
});


//===================================
// Utils
//===================================
function generateId() {
    return Date.now()
}

function generateToken(length) {
    let alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-'
    let token = ''

    for (let i = 0; i < length; i++) {
        token += alphabet[Math.floor(Math.random() * alphabet.length)]
    }

    return token
}

function sendCardAddEvent(card) {
    let response = {
        type: 'addCard',
        data: card
    }
    for (var i = 0; i < clients[response.data.boardId].length; i++) {
        if (clients[response.data.boardId][i].readyState != 3) {
            clients[response.data.boardId][i].send(JSON.stringify(response));
        }
    }
}

function sendCardUpdateEvent(card) {
    let response = {
        type: 'updateCard',
        data: card
    }
    for (var i = 0; i < clients[response.data.boardId].length; i++) {
        if (clients[response.data.boardId][i].readyState != 3) {
            clients[response.data.boardId][i].send(JSON.stringify(response));
        }
    }
}

function sendThumbsUpEvent(card) {
    let response = {
        type: 'like',
        data: card
    }
    for (var i = 0; i < clients[response.data.boardId].length; i++) {
        if (clients[response.data.boardId][i].readyState != 3) {
            clients[response.data.boardId][i].send(JSON.stringify(response));
        }
    }
}

function connectionSearch(clients, conn) {
    if (!clients.hasOwnProperty(conn.boardId)) {
        return undefined;
    }

    for (var user in clients[conn.boardId]) {
        if (clients[conn.boardId][user] === conn) {
            return conn;
        }
    }

    return undefined;
}

function addSanitizedColumnNames(boards) {
    let sanitized = []
    for(let i = 0; i < boards.columns.length; i++) {
        sanitized.push(boards.columns[i].replace(/[^a-zA-Z]/g, "-"))
    }
    boards.sanitizedColumns = sanitized
}

//===================================
// Exports
//===================================
exports.getRouter = function () {
    return router
}

exports.startWebSocketService = function (server) {
    ws = new WebSocket.Server({ server });
    ws.on('connection', (conn, req) => {
        if (req.url.length < 15) {
            console.log('Bad board id');
            return;
        }
        console.log(req.connection.remoteAddress + ' has connected');
        var boardId = req.url.slice(2)
        if (!clients.hasOwnProperty(boardId)) {
            clients[boardId] = [];
        }

        conn.boardId = boardId;
        clients[boardId].push(conn);
        console.log(boardId + ' now has ' + clients[boardId].length + ' users');

        //console.log(ws.clients.size);
        //clients.push(ws.clients.size - 1);
        conn.on('close', () => {
            var user = connectionSearch(clients, conn);
            if (user == undefined) {
                return;
            }

            clients[user.boardId].splice(clients[user.boardId].indexOf(user), 1);
            console.log("connection closing in room : " + user.boardId + ".");
            if (clients[boardId].length == 0) {
                console.log('No active sessions in board : ' + user.boardId + '.');
            }
        });

        conn.on('message', (data) => {
            var json = JSON.stringify({ type: 'message', data: data });
            console.log(json);
            for (var i = 0; i < clients[conn.boardId].length; i++) {
                try {
                    if (clients[conn.boardId][i].readyState != 3) {
                        console.log('RS: ' + clients[conn.boardId][i].readyState);
                        clients[conn.boardId][i].send(json);
                    }
                }
                catch (e) {
                    console.log(e);
                }
            }
        });
    });
}



//module.exports = router